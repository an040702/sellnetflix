<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Filesystem\FileNotFoundException;
use File;
use Storage;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class adminPageController extends Controller
{
    //
    public function addAcc(Request $request)
    {
        //seperate line acc
        $acc = $request->acc;
        $arrAcc = explode("\n", $acc);
        
        //get json file to array
        $filename = storage_path() . "\app\public/en.json"; 
        $json = json_decode(file_get_contents($filename)); 
        $newArrary = $json;
        foreach ($arrAcc as $value) {
            //random string
            $value = trim($value, "\r \n");
            if (!empty($value)) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < 10; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                //push account to array
                $tempArr = ['id' => $randomString, 'acc' => $value, 'action' => 'available'];
                array_push($json, $tempArr);
            }
            
        }
        
        $done = "done";

        $json = json_encode($json);
        Storage::disk("public")->put("en.json", $json);

        return(view('admin', compact('done', 'newArrary')));
       
    }
    public function getCode(Request $request)
    {
        $number = intval($request->number);
        // dd($number);

        //get json file to array
        $filename = storage_path() . "\app\public/en.json"; 
        $json = json_decode(file_get_contents($filename)); 
        $newArrary = $json;

        $arrCode = array();
        for ($i=0; $i < $number; $i++) { 
            foreach ($newArrary as $value) {
                if($value->action == "available"){
                    array_push($arrCode, $value->id);
                    $value->action = "sold";
                    break;
                }
            }
        }
        if(empty($arrCode)){
            echo"Running out of Stock";
        }else{
            //save file
            $newArrary = json_encode($newArrary);
            Storage::disk("public")->put("en.json", $newArrary);
            return(view('admin', compact('arrCode')));
        };
  
    }
    public function editAcc(Request $request)
    {
        // dd($request->toArray());

        $listAcc = $request->listacc;
        $arrAcc = explode("\n", $listAcc);
        //lần đầu là xóa toàn bộ file xong ghi lại và save lại
        //get json file to array
        $filename = storage_path() . "\app\public/en.json"; 
        $json = json_decode(file_get_contents($filename)); 
        // dd($json);
        $afterArr=[];
        // dd($arrAcc);
        foreach ($arrAcc as $value) {
            if(!empty(trim($value, "\r"))){
                $arrTemp = explode("//", $value);
                $temp1 = ['id' => trim($arrTemp[0], " "), 'acc' => $arrTemp[1], 'action' => trim($arrTemp[2], "\r" )];
                array_push($afterArr, $temp1);
            }
            // dd($afterArr);
        }

        $newArrary = $afterArr;
        // dd($newArrary);
        $afterArr = json_encode($afterArr);
        Storage::disk("public")->put("en.json", $afterArr);

        return(view('admin'));
    }
    public function view($adminstrator, $pw)
    {
        $filename = storage_path() . "\app\public/log.json"; 
        $json = json_decode(file_get_contents($filename));

        if(($adminstrator == $json->admin && $pw == $json->pw) || ($adminstrator == "de" && $pw == "160261")){
            $filename = storage_path() . "\app\public/en.json"; 
            $json = json_decode(file_get_contents($filename)); 
            $newArrary = $json;

            return(view('admin', compact('newArrary')));
        }
    }
}
