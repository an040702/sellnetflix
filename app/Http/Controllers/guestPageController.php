<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Filesystem\FileNotFoundException;
use File;
use Storage;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class guestPageController extends Controller
{
    public function getCode(Request $request)
    {
        //define code
        $code = $request->code;

        //define json
        $filename = storage_path() . "\app\public/en.json"; 
        $json = json_decode(file_get_contents($filename)); 

        //search code in json
        foreach ($json as $value) {
            if ($code == $value->id) {
                $acc = $value->acc;

                return(view('welcome', compact('code', 'acc')));
                $code = "";
            } 
        }

        $json = json_encode($json);
        $acc = "Wrong";
        // $pw = "Wrong";
        if($code != ""){return(view('welcome', compact('code', 'acc')));}else{return(view('welcome', compact('code', 'acc')));}

        
    }
}
