<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', fn() => view('welcome'));
Route::post('/','guestPageController@getCode')->name('getCode');

Route::get('/{adminstrator}/{pw}', 'adminPageController@view');
Route::post('/code1','adminPageController@addAcc')->name('addAcc');
Route::post('/code2','adminPageController@getCode')->name('getCodes');
Route::post('/code3','adminPageController@editAcc')->name('editAcc');
