@extends('masterPage')
@section('content')
<div class="container">
    <div class="page-title">
      <div class="row">
        <div class="col-12">
          <h1 class="text-center">Get Your Account Here</h1>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container">

    @if (empty($code))

    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5 class="text-center">Input Your Code To Get Your Account</h5>
          </div>
        <form class="form theme-form" method="POST" action="{{ Route('getCode') }}">
            @csrf
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <div class="mb-3">
                    <label class="form-label" for="exampleFormControlInput1">Your Code</label>
                    <input class="form-control" name="code" type="Text" placeholder="MIDE-937">
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer text-end">
              <button class="btn btn-primary" type="submit">Submit</button>
              <input class="btn btn-light" type="reset" value="Cancel">
            </div>
          </form>
        </div>
      </div>
    </div>
        
    @else
        {{-- return account --}}
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5 class="text-center">Your Account</h5>
          </div>
        <form class="form theme-form">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <div class="mb-3">
                    <label class="form-label" for="exampleFormControlInput1">Your Account</label>
                  <input class="form-control" name="code" type="Text"  value="Your Account: {{$acc}}">
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer text-center">
              <div class="alert alert-warning" role="alert" style="color: black;">
                <i class="fas fa-exclamation-triangle"></i>DONT CHANGE PROFILE NAME
              </div>
              <div class="alert alert-warning" role="alert" style="color: black;">
                <i class="fas fa-exclamation-triangle"></i>DONT PUT <b>PIN</b> IN PROFILE
              </div>
              <div class="alert alert-warning" role="alert" style="color: black;">
                <i class="fas fa-exclamation-triangle"></i>DONT MAKE ANY CHANGE TO THE ACCOUNT AND PROFILE
              </div>
              <div class="alert alert-warning" role="alert" style="color: black;">
                <i class="fas fa-exclamation-triangle"></i>1 PROFILE 1 DEVICE ONLY
              </div>
              <div class="alert alert-warning" role="alert" style="color: black;">
                <i class="fas fa-exclamation-triangle"></i>RECEIVE ORDER AND RATE TO ACTIVE YOUR WARRANTY
              </div>
              <div class="alert alert-warning" role="alert" style="color: black;">
                <i class="fas fa-exclamation-triangle"></i>PLEASE UPDATE YOUR NETFLIX APPS IF HAVE UPDATE
              </div>
              <div class="alert alert-danger" role="alert" style="color: black;">
                <i class="fas fa-exclamation-triangle"></i>Cheap price come with some strict rules
              </div>
            </div>
            
          </form>
        </div>
      </div>
    </div>
    @endif
    
    
  </div>
  <!-- Container-fluid Ends-->
@endsection