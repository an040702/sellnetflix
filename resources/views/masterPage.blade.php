<!DOCTYPE html>
<html lang="en">
  <head>
   @include('head')
  </head>
  <body onload="startTime()">
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
      @include('header')
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <!-- Page Sidebar Ends-->
        <div class="page-body" style="margin-left:0px; width: calc(100%);">
          @yield('content')
        </div>
        <!-- footer start-->
        @include('footer')
      </div>
    </div>
    @include('script')
  </body>
</html>