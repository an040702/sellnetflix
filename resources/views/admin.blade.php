@extends('masterPage')
@section('content')
<div class="container">
    <div class="page-title">
      <div class="row">
        <div class="col-12">
          <h1 class="text-center">Adminstrator</h1>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header">
              <h5 class="text-center">Generate Code</h5>
            </div>
          <form class="form theme-form" method="POST" action="{{ Route('getCodes') }}">
              @csrf
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <div class="mb-3">
                      <label class="form-label" for="exampleFormControlInput1">How many code you want?</label>
                      <textarea class="form-control" name="number" type="number" placeholder="Number" rows="5"></textarea>
                    </div>
                  </div>
                  
                </div>
                @if (!empty($arrCode))
                      @foreach ($arrCode as $item)
                        <div class="row">
                          <div class="col">{{$item}}</div>
                        </div>
                      @endforeach
                      <div class="alert alert-warning text-center" role="success" style="color: white;">
                        Codes will disappear when RELOAD 
                      </div>
                @endif
              </div>
              <div class="card-footer text-end">
                <button class="btn btn-primary" type="submit">Submit</button>
                <input class="btn btn-light" type="reset" value="Cancel">
              </div>
            </form>
          </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                  <h5 class="text-center">Add Account</h5>
                </div>
              <form class="form theme-form" method="POST" action="{{ Route('addAcc') }}">
                  @csrf
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <div class="mb-3">
                          <label class="form-label" for="exampleFormControlInput1">Your Account</label>
                          <textarea class="form-control" name="acc" type="Text" placeholder="MIDE-937" rows="5"></textarea>
                          @if (!empty($done))
                              <br>
                              <div class="alert alert-success text-center" role="success" style="color: white;">
                                DONE & press RELOAD to update 
                              </div>
                              {{-- <script> location.reload(); </script> --}}

                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <button class="btn btn-primary" type="submit" onsubmit="() => {location.reload();}">Submit</button>
                    <button onclick="() => {location.reload();}" class="btn btn-success">Reload</button>
                    <input class="btn btn-light" type="reset" value="Cancel">
                  </div>
                  
                </form>
              </div>
        </div>
      </div>
    </div>
    <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
                <div class="card-header">
                  <h5 class="text-center">All Account</h5>
                </div>
              <form class="form theme-form" method="POST" action="{{ Route('editAcc') }}">
                  @csrf
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <div class="mb-3 text-center">
                          <label class="form-label" for="exampleFormControlInput1">Edit or Delete</label>

                          <textarea class="form-control" name="listacc" type="Text" placeholder="MIDE-937" rows="20" >
                            @if (!empty($newArrary))
                                @foreach ($newArrary as $item)
                                  {{trim($item->id, " ")}}//{{$acc = trim(stripslashes($item->acc), "\r")}}//{{trim($item->action == "sold" ? "sold" : "available", "\r")}}
                                @endforeach
                                {{-- {{header("Refresh:1")}} --}}
                            @endif
                          </textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <button class="btn btn-primary" type="submit">Done</button>
                    <input class="btn btn-light" type="reset" value="Cancel">
                  </div>
                  
                </form>
              </div>
      </div>
    </div>
  </div>
</div>
@endsection